package com.binary_studio.dependency_detector;

import java.util.HashSet;
import java.util.List;

public final class DependencyDetector {

	private DependencyDetector() {
	}

	public static boolean canBuild(DependencyList libraries) {


		Boolean b= false;

		HashSet<String> dependency = new HashSet<String>();
		List<String> key = libraries.libraries;
		List<String[]> value = libraries.dependencies;

		if (key.isEmpty() || value.isEmpty()) {
			b = true;

		} else {

			String list[][] = value.toArray(String[][]::new);

			for (int j = 0; j < list[0].length - 1; j++) {



				for (int i = 0; i < list.length; i++) {



					if (key.get(i).equals(list[i][j])) {
						dependency.add(list[i][j + 1]);


						if (dependency.contains(key.get(i))) {
							b = false;
						}
						else {
							b=true;
						}
					}
				}

			}
		}
		return b;
	}
}