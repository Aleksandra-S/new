package com.binary_studio.academy_coin;

import java.util.stream.Stream;

public final class AcademyCoin {

	private AcademyCoin() {
	}
	public static int maxprofitt(int price[], int start, int end){

		if (end <= start)
			return 0;

		int profit = 0;
		int curr_profit =0;

		for (int i = start; i < end; i++)
		{
			for (int j = i + 1; j <= end; j++)
			{
				if (price[j] > price[i])
				{
					curr_profit = price[j] - price[i]
							+ maxprofitt(price, start, i - 1)
							+ maxprofitt(price, j + 1, end);


				}
				profit = Math.max(profit, curr_profit);
			}
		}
		return profit;

	}

	public static int maxProfit(Stream<Integer> prices) {
		// TODO: Implement

		int [] price = prices.mapToInt(Integer::intValue).toArray();

		int end = (price.length - 1);

		int profit = maxprofitt(price,0,end);

		return profit;
	}

}
