package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

	private String name;
	private PositiveInteger impactReduction;
	private PositiveInteger shieldRegen;
	private PositiveInteger hullRegen;
	private PositiveInteger capacitorUsage;
	private PositiveInteger pgRequirement;

	public DefenciveSubsystemImpl(String name, PositiveInteger powergridConsumption, PositiveInteger capacitorConsumption, PositiveInteger impactReduction,PositiveInteger shieldRegen, PositiveInteger hullRegen) {

		if (name ==null||name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		this.name = name;
		this.impactReduction = impactReduction;
		this.shieldRegen = shieldRegen;
		this.hullRegen = hullRegen;
		this.capacitorUsage = capacitorConsumption;
		this.pgRequirement = powergridConsumption;
	}

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
												   PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
												   PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {

		return new DefenciveSubsystemImpl(name,powergridConsumption,capacitorConsumption, impactReductionPercent, shieldRegeneration, hullRegeneration);
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {

		return pgRequirement;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {

		return capacitorUsage;
	}

	@Override
	public String getName() {

		return name;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		// TODO: Ваш код здесь :)
		double idm = incomingDamage.damage.value().doubleValue();
		double ipr = impactReduction.value().doubleValue();
		double d = idm - idm * ipr / 100.0;

		double damagechecked = idm * 0.05;

		double dm1;

		if (ipr > 95.0) {
			dm1 = damagechecked;

		} else {
			dm1 = d;
		}

		double dround = Math.ceil(dm1);

		Integer damage = Integer.valueOf((int) dround);
		PositiveInteger damage1 = PositiveInteger.of(damage);


		return new AttackAction(damage1, incomingDamage.attacker, incomingDamage.target, incomingDamage.weapon);
	}

	@Override
	public RegenerateAction regenerate() {

		return new RegenerateAction(shieldRegen,hullRegen);
	}

}
