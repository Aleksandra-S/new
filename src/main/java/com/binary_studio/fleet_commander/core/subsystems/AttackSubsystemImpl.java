package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {
	private String name;
	private PositiveInteger powergridRequirments;
	private PositiveInteger capacitorConsumption;
	private PositiveInteger optimalSpeed;
	private PositiveInteger optimalSize;
	private PositiveInteger baseDamage;

	public AttackSubsystemImpl(String name, PositiveInteger powergridRequirments, PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize, PositiveInteger baseDamage) {
		if (name ==null||name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		this.name = name;
		this.powergridRequirments = powergridRequirments;
		this.capacitorConsumption = capacitorConsumption;
		this.optimalSpeed = optimalSpeed;
		this.optimalSize = optimalSize;
		this.baseDamage = baseDamage;
	}

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
												PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
												PositiveInteger baseDamage) throws IllegalArgumentException {

				return new AttackSubsystemImpl(name, powergridRequirments, capacitorConsumption, optimalSpeed, optimalSize, baseDamage);
	}


	@Override
	public PositiveInteger getPowerGridConsumption() {

		return powergridRequirments;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {

		return capacitorConsumption;
	}

	@Override
	public PositiveInteger attack(Attackable target) {


		double sizeReduction = target.getSize().value()>=this.optimalSize.value()
				? 1.0
				: (target.getSize().value().doubleValue())/this.optimalSize.value();

		double speedReduction = target.getCurrentSpeed().value()<=this.optimalSpeed.value()
		        ? 1.0
				: (this.optimalSpeed.value())/(2* target.getCurrentSpeed().value().doubleValue());

		int damage = (int)Math.ceil(baseDamage.value() * Math.min(sizeReduction,speedReduction));

		return PositiveInteger.of(damage);
	}

	@Override
	public String getName() {

		return name;
	}

}
