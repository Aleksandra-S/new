package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private  String name;
	private  PositiveInteger shieldHP;
	private  PositiveInteger hullHP;
	private  PositiveInteger powergridOutput;
	private  PositiveInteger capacitorAmount;
	private  PositiveInteger capacitorRechargeRate;
	private AttackSubsystem attackSubsystem;
	private DefenciveSubsystem defenciveSubsystem;
	private PositiveInteger speed;
	private PositiveInteger size;
	private Integer curentCap;
	private Integer currentHull;
	private Integer currentShield;

	public CombatReadyShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, AttackSubsystem attack, DefenciveSubsystem defence) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.powergridOutput = powergridOutput;
		this.capacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.attackSubsystem = attack;
		this.defenciveSubsystem = defence;
		this.speed = speed;
		this.size = size;

		this.curentCap = capacitorAmount.value();
		this.currentHull =hullHP.value();
		this.currentShield =shieldHP.value();
	}

	@Override
	public void endTurn() {

			}

	@Override
	public void startTurn() {

	}

	@Override
	public String getName() {
		// TODO: Ваш код здесь :)
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		// TODO: Ваш код здесь :)
		return PositiveInteger.of(1);
	}

	@Override
	public PositiveInteger getCurrentSpeed() {

		return PositiveInteger.of(0);
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {

		if (this.curentCap < this.attackSubsystem.getCapacitorConsumption().value()) {
			return Optional.empty();
		}
		this.curentCap -= this.attackSubsystem.getCapacitorConsumption().value();
		var damage = this.attackSubsystem.attack(target);
		return Optional.of(new AttackAction(damage, this, target, this.attackSubsystem));
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {

		var damage = this.defenciveSubsystem.reduceDamage(attack);
		if (damage.damage.value() > currentShield + currentHull) {
			return new AttackResult.Destroyed();
		}
		var rest = Math.max(damage.damage.value() - Math.max(this.currentShield, 0), 0);
		this.currentShield -= damage.damage.value() - rest;
		this.currentHull -= rest;
		return new AttackResult.DamageRecived(damage.weapon, damage.damage, this);
	}
	@Override
	public Optional<RegenerateAction> regenerate() {

		if (this.curentCap<this.defenciveSubsystem.getCapacitorConsumption().value()){
						return  Optional.empty();
		}
		this.curentCap -= this.defenciveSubsystem.getCapacitorConsumption().value();

		var regen = this.defenciveSubsystem.regenerate();
		var shieldRegenerated = Math.min(this.shieldHP.value() - this.currentShield, regen.shieldHPRegenerated.value());
		var hullRegenerated = Math.min(this.hullHP.value() -this.currentHull, regen.hullHPRegenerated.value());
		this.currentHull += hullRegenerated;
		this.currentShield += shieldRegenerated;

		return Optional.of(new RegenerateAction(PositiveInteger.of(shieldRegenerated),PositiveInteger.of(hullRegenerated)));
	}

}
