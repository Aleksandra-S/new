package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.Subsystem;

import java.util.Optional;
import java.util.stream.Stream;

public final class DockedShip implements ModularVessel {

	private  String name;
	private  PositiveInteger shieldHP;
	private  PositiveInteger hullHP;
	private  PositiveInteger powergridOutput;
	private  PositiveInteger capacitorAmount;
	private  PositiveInteger capacitorRechargeRate;
	private Optional<AttackSubsystem> attackSubsystem;
	private Optional<DefenciveSubsystem> defenciveSubsystem;
	private PositiveInteger speed;
	private PositiveInteger size;

	public DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed, PositiveInteger size) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.powergridOutput = powergridOutput;
		this.capacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.attackSubsystem = Optional.empty();
		this.defenciveSubsystem = Optional.empty();
		this.speed = speed;
		this.size = size;
	}

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
									   PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
									   PositiveInteger speed, PositiveInteger size) {
		// TODO: Ваш код здесь :)
		return new DockedShip(name,shieldHP,hullHP, powergridOutput,capacitorAmount,capacitorRechargeRate,speed,size);
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		// TODO: Ваш код здесь :)
		var newAttack = Optional.ofNullable(subsystem);
		assertCanFit(Stream.of(newAttack.map(sub-> (Subsystem)sub), this.defenciveSubsystem.map(sub -> (Subsystem)sub)));
	 this.attackSubsystem =newAttack;
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		// TODO: Ваш код здесь :)
		var newDefence = Optional.ofNullable(subsystem);
		assertCanFit(Stream.of(newDefence.map(sub-> (Subsystem)sub), this.attackSubsystem.map(sub -> (Subsystem)sub)));
		this.defenciveSubsystem =newDefence;


	}
	private void assertCanFit(Stream<Optional<Subsystem>> subsystems) throws InsufficientPowergridException{
		var totalPowerGrid = subsystems.mapToInt(sub ->sub.map(s->s.getPowerGridConsumption().value()).orElse(0)).sum();
			if(totalPowerGrid>this.powergridOutput.value()){
			throw new InsufficientPowergridException(totalPowerGrid - this.powergridOutput.value());
		}
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		// TODO: Ваш код здесь :)
		if (this.attackSubsystem.isEmpty()){
			throw NotAllSubsystemsFitted.attackMissing();
		}
		if (this.defenciveSubsystem.isEmpty()){
			throw NotAllSubsystemsFitted.defenciveMissing();
		}
		return new CombatReadyShip(name,shieldHP,hullHP,powergridOutput,capacitorAmount,capacitorRechargeRate,attackSubsystem.get(), defenciveSubsystem.get());
	}

}
